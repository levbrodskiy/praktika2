package com.educationalpractice.ivabrodskiy.controllers;

import com.educationalpractice.ivabrodskiy.entities.ParentPair;
import com.educationalpractice.ivabrodskiy.services.ParentPairsService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

/**
 * Управляет представлениями информации
 * По относительной ссылке /parents и далее
 */
@Controller
@RequestMapping("/parents")
public class ParentPairsController {

    private ParentPairsService parentPairsService = new ParentPairsService();

    @GetMapping
    public String getParents(Model model) {
        model.addAttribute("parents", parentPairsService.findAllParentPairs());
        return "parents";
    }

    @GetMapping("/edit{id}")
    public String editGet(@PathVariable(name = "id")Long id,
                          Model model) {
        ParentPair parentPair = parentPairsService.findParentPairById(id);
        model.addAttribute("parentPair", parentPair);
        return "edit_parent_pair";
    }

    @PostMapping("/edit{id}")
    public String editPost(@ModelAttribute ParentPair parentPair) {
        parentPairsService.update(parentPair);
        return "redirect:/parents";
    }

    @GetMapping("/add")
    public String addGet() {
        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
        return "addParents";
    }

    @PostMapping("/add")
    public String addPost(@ModelAttribute ParentPair parentPair) {
        try {
            parentPair.setDadPhoneNumber(null);
            parentPair.setMomPhoneNumber(null);
            parentPairsService.save(parentPair);
        }catch (Exception e){
            e.printStackTrace();
            return "error";
        }

        return "redirect:/parents";
    }

    @PostMapping("/delete{id}")
    public String delete(@ModelAttribute ParentPair parentPair,
                         Model model) {
        parentPairsService.delete(parentPair);
        return "edit_student";
    }
}
