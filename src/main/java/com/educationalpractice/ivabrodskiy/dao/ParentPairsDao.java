package com.educationalpractice.ivabrodskiy.dao;

import com.educationalpractice.ivabrodskiy.entities.ParentPair;
import com.educationalpractice.ivabrodskiy.utils.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.springframework.stereotype.Component;
import java.util.List;

/**
 * Предоставляет методы достуа к БД
 * Таблица : parent_pairs
 */
@Component
public class ParentPairsDao {
    public ParentPair findById(long id) {
        return HibernateUtil.getSessionFactory().openSession().get(ParentPair.class, id);
    }

    public void save(ParentPair parentPair) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.save(parentPair);
        tx1.commit();
        session.close();
    }

    public void update(ParentPair parentPair) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.update(parentPair);
        tx1.commit();
        session.close();
    }

    public void delete(ParentPair parentPair) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.delete(parentPair);
        tx1.commit();
        session.close();
    }

    public ParentPair findByStudentCode(long studentCode) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Query query = session.createQuery("From ParentPair WHERE student_code = :code", ParentPair.class);
        query.setParameter("code", studentCode);
        ParentPair parentPair = (ParentPair) query.getSingleResult();
        return parentPair;
    }

    @SuppressWarnings("all")
    public List<ParentPair> findAll() {
        List<ParentPair> parentPairs = (List<ParentPair>)  HibernateUtil.getSessionFactory().openSession().createQuery("From ParentPair").list();
        return parentPairs;
    }
}
