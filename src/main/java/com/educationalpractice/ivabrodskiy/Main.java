package com.educationalpractice.ivabrodskiy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public abstract class Main {
    public static void main(String[] args) {
        SpringApplication.run(Main.class, args);
    }
}
