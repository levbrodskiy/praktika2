package com.educationalpractice.ivabrodskiy.controllers;

import com.educationalpractice.ivabrodskiy.entities.Subject;
import com.educationalpractice.ivabrodskiy.services.SubjectsService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

/**
 * Управляет представлениями информации
 * По относительной ссылке /subjects и далее
 */
@Controller
@RequestMapping("/subjects")
public class SubjectsController {

    private SubjectsService subjectsService = new SubjectsService();

    @GetMapping
    private String getSubjects(Model model) {
        model.addAttribute("subjects", subjectsService.findAllSubjects());
        return "subjects";
    }

    @GetMapping("/add")
    private String getAddSubjectPage() {
        return "addSubject";
    }

    @PostMapping("/add")
    public String addSubject(@ModelAttribute Subject subject, Model model) {
        if (subjectsService.save(subject)) {
            return "redirect:/subjects";
        } else {
            model.addAttribute("errorTitle", "Ошибка добавления");
            model.addAttribute("errorDescription", "Неверные данные");
            return "error";
        }
    }

    @GetMapping("{id}/edit")
    public String getEditSubjectPage(@PathVariable Long id) {
        return "editSubject";
    }

    @PostMapping("{id}/edit")
    public String editSubject(@ModelAttribute Subject subject,
                              @PathVariable Long id,
                              Model model) {
        try {
            subjectsService.update(subject);
            return "redirect:/subjects";
        } catch (Exception e) {
            model.addAttribute("errorTitle", "Ошибка обновления");
            model.addAttribute("errorDescription", "Неверные данные");
            return "error";
        }
    }

    @PostMapping("{id}/delete")
    public String deleteSubject(@PathVariable Long id, Model model) {
        try {
            subjectsService.delete(subjectsService.findSubjectById(id));
            return "redirect:/subjects";
        } catch (Exception e) {
            model.addAttribute("errorTitle", "Ошибка удаления");
            model.addAttribute("errorDescription", "");
            return "error";
        }
    }
}
