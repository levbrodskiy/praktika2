package com.educationalpractice.ivabrodskiy.templates;

import com.educationalpractice.ivabrodskiy.entities.ProgressJournalRecord;
import com.educationalpractice.ivabrodskiy.entities.Student;
import com.educationalpractice.ivabrodskiy.entities.Subject;
import lombok.Data;

/**
 * Шаблон для отправки данных в представление журнала успеваемости
 */
@Data
public class ProgressJournalTemplate {
    private ProgressJournalRecord progressJournalRecord;
    private Student student;
    private Subject subject;

    public ProgressJournalTemplate(ProgressJournalRecord progressJournalRecord, Student student, Subject subject) {
        this.progressJournalRecord = progressJournalRecord;
        this.student = student;
        this.subject = subject;
    }
}
