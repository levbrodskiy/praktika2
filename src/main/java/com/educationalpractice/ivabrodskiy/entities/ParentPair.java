package com.educationalpractice.ivabrodskiy.entities;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "parents")
@Data
public class ParentPair {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "student_code")
    private Long studentCode;

    @Column(name = "family_status")
    private String familyStatus;

    @Column(name = "kids_count")
    private Integer kidsCount;

    @Column(name = "mom_first_name")
    private String momFirstName;

    @Column(name = "mom_middle_name")
    private String momMiddleName;

    @Column(name = "mom_last_name")
    private String momLastName;

    @Column(name = "mom_city")
    private String momCity;

    @Column(name = "mom_street")
    private String momStreet;

    @Column(name = "mom_house_number")
    private Integer momHouseNumber;

    @Column(name = "mom_flat_number")
    private Integer momFlatNumber;

    @Column(name = "mom_phone_number")
    private Long momPhoneNumber;

    @Column(name = "mom_workplace")
    private String momWorkPlace;

    @Column(name = "dad_first_name")
    private String dadFirstName;

    @Column(name = "dad_middle_name")
    private String dadMiddleName;

    @Column(name = "dad_last_name")
    private String dadLastName;

    @Column(name = "dad_city")
    private String dadCity;

    @Column(name = "dad_street")
    private String dadStreet;

    @Column(name = "dad_house_number")
    private Integer dadHouseNumber;

    @Column(name = "dad_flat_number")
    private Integer dadFlatNumber;

    @Column(name = "dad_phone_number")
    private Long dadPhoneNumber;

    @Column(name = "dad_workplace")
    private String dadWorkPlace;
}
