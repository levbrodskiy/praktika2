package com.educationalpractice.ivabrodskiy.services;

import com.educationalpractice.ivabrodskiy.dao.AttendanceJournalDao;
import com.educationalpractice.ivabrodskiy.entities.AttendanceJournalRecord;
import java.util.List;

/**
 * Сервис, предоставляющий методы для работы с журналом посещаемости
 */
public class AttendanceJournalsService {

    private AttendanceJournalDao attendanceJournalDao = new AttendanceJournalDao();

    public AttendanceJournalRecord findAttendanceJournalRecordById(long id) {
        return attendanceJournalDao.findById(id);
    }

    public boolean save(AttendanceJournalRecord attendanceJournalRecord) {
        attendanceJournalDao.save(attendanceJournalRecord);
        return true;
    }

    public void update(AttendanceJournalRecord attendanceJournalRecord) {
        attendanceJournalDao.update(attendanceJournalRecord);
    }

    public void delete(AttendanceJournalRecord attendanceJournalRecord) {
        attendanceJournalDao.delete(attendanceJournalRecord);
    }

    public List<AttendanceJournalRecord> findAllAttendanceJournalRecords() {
        return attendanceJournalDao.findAll();
    }
}
