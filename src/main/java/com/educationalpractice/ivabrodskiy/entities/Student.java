package com.educationalpractice.ivabrodskiy.entities;

import lombok.Data;
import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "students")
@Data
public class Student {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "student_code", nullable = false)
    private Long studentCode;

    @Column(name = "first_name", nullable = false)
    private String firstName;

    @Column(name = "middle_name")
    private String middleName;

    @Column(name = "last_name", nullable = false)
    private String lastName;

    @Temporal(TemporalType.DATE)
    @Column(name = "birth_date", nullable = false)
    private Date birthDate;

    @Column(name = "city", nullable = false)
    private String city;

    @Column(name = "street", nullable = false)
    private String street;

    @Column(name = "house_number", nullable = false)
    private Integer houseNumber;

    @Column(name = "flat_number")
    private Integer flatNumber;

    @Column(name = "phone_number")
    private Long phoneNumber;

    @Column(name = "passport_series", nullable = false)
    private Long passportSeries;

    @Column(name = "passport_number", nullable = false)
    private Long passportNumber;

    @Column(name = "birth_city")
    private String birthCity;

    @Column(name = "passport_by_whom", nullable = false)
    private String passportByWhom;

    @Temporal(TemporalType.DATE)
    @Column(name = "passport_date_issue", nullable = false)
    private Date passportDateIssue;

    @Column(name = "division_code", nullable = false)
    private Long divisionCode;

    @Column(name = "INN", nullable = false)
    private Long INN;

    @Column(name = "SNILS_number", nullable = false)
    private Long SNILSNumber;

    @Column(name = "birth_certificate_series", nullable = false)
    private Long birthCertificateSeries;

    @Column(name = "birth_certificate_number", nullable = false)
    private Long birthCertificateNumber;

    @Column(name = "birth_certificate_by_whom", nullable = false)
    private String  birthCertificateByWhom;

    @Temporal(TemporalType.DATE)
    @Column(name = "birth_certificate_date_issue", nullable = false)
    private Date birthCertificateDateIssue;
}
