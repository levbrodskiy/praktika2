package com.educationalpractice.ivabrodskiy.dao;

import com.educationalpractice.ivabrodskiy.entities.Student;
import com.educationalpractice.ivabrodskiy.utils.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.stereotype.Component;
import javax.persistence.Query;
import java.util.List;

/**
 * Предоставляет методы достуа к БД
 * Таблица : students
 */
@Component
public class StudentsDao {
    public Student findById(long id) {
        return HibernateUtil.getSessionFactory().openSession().get(Student.class, id);
    }

    public Student findByStudentCode(long studentCode) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Query query = session.createQuery("FROM Student WHERE studentCode = :code");
        query.setParameter("code", studentCode);
        return (Student) query.getSingleResult();
    }

    public void save(Student student) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.save(student);
        tx1.commit();
        session.close();
    }

    public void update(Student student) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.update(student);
        tx1.commit();
        session.close();
    }

    public void delete(Student student) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.delete(student);
        tx1.commit();
        session.close();
    }

    public List<Student> findAll() {
        List<Student> students = (List<Student>)  HibernateUtil.getSessionFactory().openSession().createQuery("From Student").list();
        return students;
    }
}
