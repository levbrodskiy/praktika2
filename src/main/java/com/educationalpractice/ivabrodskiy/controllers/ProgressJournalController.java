package com.educationalpractice.ivabrodskiy.controllers;

import com.educationalpractice.ivabrodskiy.entities.ProgressJournalRecord;
import com.educationalpractice.ivabrodskiy.services.ProgressJournalService;
import com.educationalpractice.ivabrodskiy.services.StudentsService;
import com.educationalpractice.ivabrodskiy.services.SubjectsService;
import com.educationalpractice.ivabrodskiy.templates.ProgressJournalTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Управляет представлениями информации
 * По относительной ссылке /progress-journal и далее
 */
@Controller
@RequestMapping("/progress-journal")
public class ProgressJournalController {
    private final ProgressJournalService progressJournalService = new ProgressJournalService();
    private final StudentsService studentsService = new StudentsService();
    private final SubjectsService subjectsService = new SubjectsService();

    @GetMapping
    public String getJournal(Model model) {
        List<ProgressJournalRecord> records = progressJournalService.findAllRecords();
        List<ProgressJournalTemplate> templates = new ArrayList<>();

        for (ProgressJournalRecord record : records) {
            templates.add(new ProgressJournalTemplate(record,
                    studentsService.findStudentById(record.getStudentID()),
                    subjectsService.findSubjectById(record.getSubjectID())));
        }
        model.addAttribute("progressJournal", templates);
        return "progressJournal";
    }

    @GetMapping("/record/add")
    public String getAddPage() {
        return "progressJournalAddPage";
    }

    @PostMapping("/record/add")
    public String addProgressJournalRecord(@ModelAttribute ProgressJournalRecord progressJournalRecord,
                                           Model model) {
        try {
            progressJournalService.save(progressJournalRecord);
        } catch (Exception e) {
            model.addAttribute("errorTitle", "Неверные данные");
            return "error";
        }
        return "redirect:/progress-journal";
    }

    @GetMapping("/record{id}/edit")
    public String getEditPage() {
        return "progressJournalEditPage";
    }

    @PostMapping("/record{id}/edit")
    public String editProgressJournalRecord(@ModelAttribute ProgressJournalRecord record,
                                            Model model) {
        try {
            progressJournalService.update(record);
        } catch (Exception e) {
            model.addAttribute("errorTitle", "Неверные данные");
            return "error";
        }
        return "redirect:/progress-journal";
    }

    @PostMapping("/record{id}/delete")
    public String deleteRecord(@PathVariable(name = "id")Long id,
                               Model model) {
        try {
            progressJournalService.delete(progressJournalService.findById(id));
        } catch (Exception e) {
            model.addAttribute("errorTitle", "Неверные данные");
            return "error";
        }
        return "redirect:/progress-journal";
    }
}
