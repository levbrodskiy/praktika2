package com.educationalpractice.ivabrodskiy.entities;

import lombok.Data;
import javax.persistence.*;

@Entity
@Table(name = "subjects")
@Data
public class Subject {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "teacher_first_name")
    private String teacherFirstName;

    @Column(name = "teacher_middle_name")
    private String teacherMiddleName;

    @Column(name = "teacher_last_name")
    private String teacherLastName;

}
