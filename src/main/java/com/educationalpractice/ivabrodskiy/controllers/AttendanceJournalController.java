package com.educationalpractice.ivabrodskiy.controllers;

import com.educationalpractice.ivabrodskiy.entities.AttendanceJournalRecord;
import com.educationalpractice.ivabrodskiy.services.AttendanceJournalsService;
import com.educationalpractice.ivabrodskiy.services.StudentsService;
import com.educationalpractice.ivabrodskiy.services.SubjectsService;
import com.educationalpractice.ivabrodskiy.templates.AttendanceJournalTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.ArrayList;
import java.util.List;

/**
 * Управляет представлениями информации
 * По относительной ссылке /attendance-journal и далее
 */
@Controller
@RequestMapping("/attendance-journal")
public class AttendanceJournalController {
    private AttendanceJournalsService attendanceJournalsService = new AttendanceJournalsService();
    private StudentsService studentsService = new StudentsService();
    private SubjectsService subjectsService = new SubjectsService();

    @GetMapping
    public String getAttendanceJournal(Model model) {
        List<AttendanceJournalTemplate> templates = new ArrayList<>();

        for (AttendanceJournalRecord record : attendanceJournalsService.findAllAttendanceJournalRecords()) {
            templates.add(new AttendanceJournalTemplate(record,
                    studentsService.findStudentById(record.getStudentId()),
                    subjectsService.findSubjectById(record.getSubjectId())));
        }
        model.addAttribute("attendanceJournalRecords",
                templates);
        return "attendanceJournal";
    }

    @GetMapping("/add")
    public String getAddAttendanceJournalRecordPage() {
        return "addAddAttendanceJournalRecord";
    }

    @PostMapping("/add")
    public String addAttendanceJournalRecord(
            @ModelAttribute AttendanceJournalRecord attendanceJournalRecord,
            Model model) {
        try {
            attendanceJournalsService.save(attendanceJournalRecord);
            return "redirect:/attendanceJournal";
        } catch (Exception e) {
            model.addAttribute("errorTitle", "Ошибка добавления");
            model.addAttribute("errorDescription", "Неверные данные");
            return "error";
        }
    }
    
    @GetMapping("/record{id}/edit")
    public String getEditAttendanceJournalRecordPage() {
        return "editRecord";
    }

    @PostMapping("/record{id}/edit")
    public String editAttendanceJournalRecord(
            @ModelAttribute AttendanceJournalRecord attendanceJournalRecord,
                                              Model model) {
        try {
            attendanceJournalsService.update(attendanceJournalRecord);
        } catch (Exception e) {
            model.addAttribute("errorTitle", "Неверные данные");
            return "error";
        }
        return "redirect:/attendance-journal";
    }
}
