package com.educationalpractice.ivabrodskiy.services;

import com.educationalpractice.ivabrodskiy.dao.ProgressJournalRecordDao;
import com.educationalpractice.ivabrodskiy.entities.ProgressJournalRecord;
import java.util.List;

/**
 * Сервис, предоставляющий методы для работы с журналом успеваемости
 */
public class ProgressJournalService {
    private ProgressJournalRecordDao progressJournalRecordDao = new ProgressJournalRecordDao();

    public ProgressJournalRecord findById(long id) {
        return progressJournalRecordDao.findById(id);
    }

    public boolean save(ProgressJournalRecord progressJournalRecord) {
        progressJournalRecordDao.save(progressJournalRecord);
        return true;
    }

    public void update(ProgressJournalRecord progressJournalRecord) {
        progressJournalRecordDao.update(progressJournalRecord);
    }

    public void delete(ProgressJournalRecord progressJournalRecord) {
        progressJournalRecordDao.delete(progressJournalRecord);
    }

    public List<ProgressJournalRecord> findAllRecords() {
        return progressJournalRecordDao.findAll();
    }
}
