package com.educationalpractice.ivabrodskiy.entities.innertypes;

public enum FamilyStatus {
    FULL, NOT_FULL;
}
