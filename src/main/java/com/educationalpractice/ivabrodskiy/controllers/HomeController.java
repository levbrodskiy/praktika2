package com.educationalpractice.ivabrodskiy.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Управляет представлениями информации
 * По относительной ссылке /home и далее
 */
@Controller
@RequestMapping("/home")
public class HomeController {

    @GetMapping
    public String getHomePage(){
        return "home";
    }
}
