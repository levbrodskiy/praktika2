package com.educationalpractice.ivabrodskiy.services;

import com.educationalpractice.ivabrodskiy.dao.ParentPairsDao;
import com.educationalpractice.ivabrodskiy.dao.StudentsDao;
import com.educationalpractice.ivabrodskiy.entities.ParentPair;
import org.springframework.stereotype.Service;
import java.util.List;

/**
 * Сервис, предоставляющий методы для работы с родителями
 */
@Service
public class ParentPairsService {

    private ParentPairsDao parentPairsDao = new ParentPairsDao();

    private StudentsDao studentsDao = new StudentsDao();

    public ParentPair findParentPairById(long id) {
        return parentPairsDao.findById(id);
    }

    public boolean save(ParentPair parentPair) {
        if (studentsDao.findByStudentCode(parentPair.getStudentCode()) == null) {
            return false;
        }
        parentPairsDao.save(parentPair);
        return true;
    }

    public void update(ParentPair parentPair) {
        parentPairsDao.update(parentPair);
    }

    public void delete(ParentPair parentPair) {
        parentPairsDao.delete(parentPair);
    }

    public List<ParentPair> findAllParentPairs() {
        return parentPairsDao.findAll();
    }
}
