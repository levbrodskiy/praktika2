package com.educationalpractice.ivabrodskiy.services;

import com.educationalpractice.ivabrodskiy.dao.StudentsDao;
import com.educationalpractice.ivabrodskiy.entities.Student;
import org.springframework.stereotype.Service;
import java.util.List;

/**
 * Сервис, предоставляющий методы для работы со студентами
 */
@Service
public class StudentsService {
    private StudentsDao studentsDao = new StudentsDao();

    public Student findStudentById(long id) {
        return studentsDao.findById(id);
    }

    public Student findStudentByCode(long studentCode) {
        return studentsDao.findByStudentCode(studentCode);
    }

    public List<Student> findAll() {
        return studentsDao.findAll();
    }

    public void save(Student student) {
        studentsDao.save(student);
    }

    public void update(Student student) {
        studentsDao.update(student);
    }

    public void delete(Student student) {
        studentsDao.delete(student);
    }
}
