package com.educationalpractice.ivabrodskiy.dao;

import com.educationalpractice.ivabrodskiy.entities.Subject;
import com.educationalpractice.ivabrodskiy.utils.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;
import java.util.List;

/**
 * Предоставляет методы достуа к БД
 * Таблица : subjects
 */
public class SubjectDao {
    public Subject findById(long id) {
        return HibernateUtil.getSessionFactory().openSession().get(Subject.class, id);
    }

    public void save(Subject subject) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.save(subject);
        tx1.commit();
        session.close();
    }

    public void update(Subject subject) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.update(subject);
        tx1.commit();
        session.close();
    }

    public void delete(Subject subject) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.delete(subject);
        tx1.commit();
        session.close();
    }

    @SuppressWarnings("unchecked cast")
    public List<Subject> findAll() {
        return (List<Subject>)
                HibernateUtil.
                        getSessionFactory().
                        openSession().
                        createQuery("From Subject").
                        list();
    }
}
