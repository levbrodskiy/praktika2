package com.educationalpractice.ivabrodskiy.services;

import com.educationalpractice.ivabrodskiy.dao.SubjectDao;
import com.educationalpractice.ivabrodskiy.entities.Subject;
import java.util.List;

/**
 * Сервис, предоставляющий методы для работы с предметами
 */
public class SubjectsService {

    private SubjectDao subjectDao = new SubjectDao();

    public Subject findSubjectById(long id) {
        return subjectDao.findById(id);
    }

    public boolean save(Subject subject) {
        try {
            subjectDao.save(subject);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public void update(Subject subject) {
        subjectDao.update(subject);
    }

    public void delete(Subject subject) {
        subjectDao.delete(subject);
    }

    public List<Subject> findAllSubjects() {
        return subjectDao.findAll();
    }
}
