package com.educationalpractice.ivabrodskiy.dao;

import com.educationalpractice.ivabrodskiy.entities.AttendanceJournalRecord;
import com.educationalpractice.ivabrodskiy.utils.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;
import java.util.List;

/**
 * Предоставляет методы достуа к БД
 * Таблица : attendance_journal
 */
public class AttendanceJournalDao {
    public AttendanceJournalRecord findById(long id) {
        return HibernateUtil.getSessionFactory().openSession().get(AttendanceJournalRecord.class, id);
    }

    public void save(AttendanceJournalRecord attendanceJournalRecord) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.save(attendanceJournalRecord);
        tx1.commit();
        session.close();
    }

    public void update(AttendanceJournalRecord attendanceJournalRecord) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.update(attendanceJournalRecord);
        tx1.commit();
        session.close();
    }

    public void delete(AttendanceJournalRecord attendanceJournalRecord) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.delete(attendanceJournalRecord);
        tx1.commit();
        session.close();
    }

    public List<AttendanceJournalRecord> findAll() {
        List<AttendanceJournalRecord> attendanceJournalRecords = (List<AttendanceJournalRecord>)  HibernateUtil.getSessionFactory().openSession().createQuery("From Student").list();
        return attendanceJournalRecords;
    }
}
