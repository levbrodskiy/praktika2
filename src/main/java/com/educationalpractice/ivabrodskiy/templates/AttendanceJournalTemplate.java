package com.educationalpractice.ivabrodskiy.templates;

import com.educationalpractice.ivabrodskiy.entities.AttendanceJournalRecord;
import com.educationalpractice.ivabrodskiy.entities.Student;
import com.educationalpractice.ivabrodskiy.entities.Subject;

/**
 * Шаблон для отправки данных в представление журнала посещаемости
 */
public class AttendanceJournalTemplate {
    private AttendanceJournalRecord attendanceJournalRecord;
    private Student student;
    private Subject subject;

    public AttendanceJournalTemplate(AttendanceJournalRecord attendanceJournalRecord, Student student, Subject subject) {
        this.attendanceJournalRecord = attendanceJournalRecord;
        this.student = student;
        this.subject = subject;
    }

    public AttendanceJournalRecord getAttendanceJournalRecord() {
        return attendanceJournalRecord;
    }

    public void setAttendanceJournalRecord(AttendanceJournalRecord attendanceJournalRecord) {
        this.attendanceJournalRecord = attendanceJournalRecord;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public Subject getSubject() {
        return subject;
    }

    public void setSubject(Subject subject) {
        this.subject = subject;
    }

    /*private Long id;
    private Date attendanceDate;
    private Boolean isAttend;
    private Long studentCode;
    private String studentFirstName;
    private String studentMiddleName;
    private String studentLastName;
    private String subjectName;
    private String teacherFirstName;
    private String teacherMiddleName;
    private String teacherLastName;*/
}
