package com.educationalpractice.ivabrodskiy.entities;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "progress_journal")
@Data
public class ProgressJournalRecord {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "student_id")
    private Long studentID;

    @Column(name = "subject_id")
    private Long subjectID;

    @Column(name = "rating")
    private Long rating;

    @Column(name = "lesson_date")
    @Temporal(value = TemporalType.DATE)
    private Date lessonDate;
}
