package com.educationalpractice.ivabrodskiy.dao;

import com.educationalpractice.ivabrodskiy.entities.ProgressJournalRecord;
import com.educationalpractice.ivabrodskiy.utils.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;
import java.util.List;

/**
 * Предоставляет методы достуа к БД
 * Таблица : progress_journal
 */
public class ProgressJournalRecordDao {
    public ProgressJournalRecord findById(long id) {
        return HibernateUtil.getSessionFactory().openSession().get(ProgressJournalRecord.class, id);
    }

    public void save(ProgressJournalRecord progressJournalRecord) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.save(progressJournalRecord);
        tx1.commit();
        session.close();
    }

    public void update(ProgressJournalRecord progressJournalRecord) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.update(progressJournalRecord);
        tx1.commit();
        session.close();
    }

    public void delete(ProgressJournalRecord progressJournalRecord) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.delete(progressJournalRecord);
        tx1.commit();
        session.close();
    }

    public List<ProgressJournalRecord> findAll() {
        List<ProgressJournalRecord> progressJournalRecords = (List<ProgressJournalRecord>)  HibernateUtil.getSessionFactory().openSession().createQuery("From Student").list();
        return progressJournalRecords;
    }
}
