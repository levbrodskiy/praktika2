package com.educationalpractice.ivabrodskiy.controllers;

import com.educationalpractice.ivabrodskiy.entities.Student;
import com.educationalpractice.ivabrodskiy.services.StudentsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Управляет представлениями информации
 * По относительной ссылке /students и далее
 */
@Controller
@RequestMapping("/students")
public class StudentsController {

    private StudentsService studentsService = new StudentsService();

    @GetMapping
    public String getStudents(Model model) {
        model.addAttribute("students", studentsService.findAll());
        return "students";
    }

    @GetMapping("/edit/{id}")
    public String editGet(@PathVariable(name = "id")Long id,
                          Model model) {
        System.out.println(id);
        Student student = studentsService.findStudentById(id);
        model.addAttribute("student", student);
        return "editStudents";
    }

    @PostMapping("/edit/{id}")
    public String editPost(Model model,
                           @PathVariable(name = "id") Long id,
                           @RequestParam(name = "studentCode") Long studentCode,
                           @RequestParam(name = "firstName") String firstName,
                           @RequestParam(name = "middleName") String middleName,
                           @RequestParam(name = "lastName") String lastName,
                           @RequestParam(name = "city") String city,
                           @RequestParam(name = "street") String street,
                           @RequestParam(name = "houseNumber") Integer houseNumber,
                           @RequestParam(name = "flatNumber") Integer flatNumber,
                           @RequestParam(name = "phoneNumber") Long phoneNumber,
                           @RequestParam(name = "passportSeries") Long passportSeries,
                           @RequestParam(name = "passportNumber") Long passportNumber,
                           @RequestParam(name = "birthCity") String birthCity,
                           @RequestParam(name = "passportByWhom") String passportByWhom,
                           @RequestParam(name = "divisionCode") Long divisionCode,
                           @RequestParam(name = "INN") Long INN,
                           @RequestParam(name = "SNILSNumber") Long SNILSNumber,
                           @RequestParam(name = "birthCertificateSeries") Long birthCertificateSeries,
                           @RequestParam(name = "birthCertificateNumber") Long birthCertificateNumber,
                           @RequestParam(name = "birthCertificateByWhom") String birthCertificateByWhom,
                           @RequestParam(name = "birthDate") String  birthDate,
                           @RequestParam(name = "passportDateIssue") String passportDateIssue,
                           @RequestParam(name = "birthCertificateDateIssue") String  birthCertificateDateIssue) {

        Student student = new Student();
        student.setId(id);
        student.setStudentCode(studentCode);
        student.setStreet(street);
        student.setFirstName(firstName);
        student.setMiddleName(middleName);
        student.setLastName(lastName);
        student.setCity(city);
        student.setHouseNumber(houseNumber);
        student.setFlatNumber(flatNumber);
        student.setPassportSeries(passportSeries);

        student.setPhoneNumber(null);
        student.setPassportByWhom(passportByWhom);
        student.setPassportNumber(passportNumber);
        student.setDivisionCode(divisionCode);
        student.setINN(INN);
        student.setSNILSNumber(SNILSNumber);
        student.setBirthCity(birthCity);
        student.setBirthCertificateSeries(birthCertificateSeries);
        student.setBirthCertificateNumber(birthCertificateNumber);
        student.setBirthCertificateByWhom(birthCertificateByWhom);

        Date birthDateStudent = new Date();
        Date passportDateIssueStudent = new Date();
        Date birthCertificateDateIssueStudent = new Date();

        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
        try {
            birthDateStudent = format.parse(birthDate);
            passportDateIssueStudent = format.parse(passportDateIssue);
            birthCertificateDateIssueStudent = format.parse(birthCertificateDateIssue);

        } catch (ParseException e) {
            model.addAttribute("errorTitle", "Неверные входные данные!");
            return "redirect:/error";
        }
        student.setBirthDate(birthDateStudent);
        student.setPassportDateIssue(passportDateIssueStudent);
        student.setBirthCertificateDateIssue(birthCertificateDateIssueStudent);
        try {
            studentsService.update(student);
        }catch (Exception e) {
            model.addAttribute("errorTitle", "Неверные входные данные!");
            return "redirect:/error";
        }

        return "redirect:/students";
    }

    @GetMapping("/add")
    public String addGet() {
        //studentsService.save(student);
        return "addStudents";
    }

    @PostMapping("/add")
    public String addPost(Model model,
                          @RequestParam(name = "studentCode") Long studentCode,
                          @RequestParam(name = "firstName") String firstName,
                          @RequestParam(name = "middleName") String middleName,
                          @RequestParam(name = "lastName") String lastName,
                          @RequestParam(name = "city") String city,
                          @RequestParam(name = "street") String street,
                          @RequestParam(name = "houseNumber") Integer houseNumber,
                          @RequestParam(name = "flatNumber") Integer flatNumber,
                          @RequestParam(name = "phoneNumber") Long phoneNumber,
                          @RequestParam(name = "passportSeries") Long passportSeries,
                          @RequestParam(name = "passportNumber") Long passportNumber,
                          @RequestParam(name = "birthCity") String birthCity,
                          @RequestParam(name = "passportByWhom") String passportByWhom,
                          @RequestParam(name = "divisionCode") Long divisionCode,
                          @RequestParam(name = "INN") Long INN,
                          @RequestParam(name = "SNILSNumber") Long SNILSNumber,
                          @RequestParam(name = "birthCertificateSeries") Long birthCertificateSeries,
                          @RequestParam(name = "birthCertificateNumber") Long birthCertificateNumber,
                          @RequestParam(name = "birthCertificateByWhom") String birthCertificateByWhom,
                          @RequestParam(name = "birthDate") String  birthDate,
                          @RequestParam(name = "passportDateIssue") String passportDateIssue,
                          @RequestParam(name = "birthCertificateDateIssue") String  birthCertificateDateIssue){

        Student student = new Student();
        student.setStudentCode(studentCode);
        student.setStreet(street);
        student.setFirstName(firstName);
        student.setMiddleName(middleName);
        student.setLastName(lastName);
        student.setCity(city);
        student.setHouseNumber(houseNumber);
        student.setFlatNumber(flatNumber);
        student.setPassportSeries(passportSeries);

        student.setPhoneNumber(null);
        student.setPassportByWhom(passportByWhom);
        student.setPassportNumber(passportNumber);
        student.setDivisionCode(divisionCode);
        student.setINN(INN);
        student.setSNILSNumber(SNILSNumber);
        student.setBirthCity(birthCity);
        student.setBirthCertificateSeries(birthCertificateSeries);
        student.setBirthCertificateNumber(birthCertificateNumber);
        student.setBirthCertificateByWhom(birthCertificateByWhom);

        Date birthDateStudent = new Date();
        Date passportDateIssueStudent = new Date();
        Date birthCertificateDateIssueStudent = new Date();

        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
        try {
            birthDateStudent = format.parse(birthDate);
            passportDateIssueStudent = format.parse(passportDateIssue);
            birthCertificateDateIssueStudent = format.parse(birthCertificateDateIssue);

        } catch (ParseException e) {
            System.out.println("parse");
            e.printStackTrace();
        }
        student.setBirthDate(birthDateStudent);
        student.setPassportDateIssue(passportDateIssueStudent);
        student.setBirthCertificateDateIssue(birthCertificateDateIssueStudent);
        try {
            studentsService.save(student);
        }catch (Exception e) {
            System.out.println("exception");
            e.printStackTrace();
        }

        return "redirect:/students";
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable(name = "id") Long id) {
        studentsService.delete(studentsService.findStudentById(id));
        return "redirect:/students";
    }
}
